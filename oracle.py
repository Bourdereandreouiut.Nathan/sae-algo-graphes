import requettes

def Menu():
    choisi = -1
    data = -1
    listSet = ['1','2','3','4','5']
    run = True
    print("Bienvenue dans le système d'Oracle Bacon de l'IUT\n")
    while(run):
        while(choisi==-1):
            print("Veuillez sélectionner une action : \n")
            print("[1] - Collaborateurs Communs\n")
            print("[2] - Collaborateurs Proches\n")
            print("[3] - Centralitée Acteur\n")
            print("[4] - Eloignement Max\n")
            print("[5] - Quitter")
            choisi = input("Faites votre sélection : ")
            if choisi == '5':
                run = False
                data = 0
                print("Aurevoir !")
            if(choisi not in listSet):
                print("Ce n'est pas un choix valide !")
                choisi = -1
        while(data==-1):
            print("Sélectionnez le nombre de films que vous souhaitez utiliser : \n")
            print("[1] - 2 films\n")
            print("[2] - 5 films\n")
            print("[3] - 10 films\n")
            print("[4] - 20 films\n")
            print("[5] - Revenir en arrière")
            data = input("Faites votre sélection (le programme prendra du temps si il y a plus de films à traiter) : ")
            if(data not in listSet):
                print("Ce n'est pas un choix valide !")
                data = -1
        data = int(data)
        choisi = int(choisi)
        if(data==1):
            Graph = requettes.json_vers_nx("./dataDeux.json")
        elif(data==2):
            Graph = requettes.json_vers_nx("./dataCinq.json")
        elif(data==3):
            Graph = requettes.json_vers_nx("./dataDix.json")
        elif(data==4):
            Graph = requettes.json_vers_nx("./dataVingt.json")
        elif(data==5):
                    choisi = -1
                    data = -1
        elif(data==69):
            Graph = requettes.json_vers_nx("./data_exploitable.json")

        if(choisi==1):
            print("Indiquez les collaborateurs voulus : ")
            aff = input("Souhaitez vous voir la liste des collaborateurs disponibles ? (Y/N)\n")
            if(aff == "Y" or aff == "y"):
                print(requettes.nodesInGraph(Graph))
            print("\n")
            Collab1 = input("Collaborateur 1 : ")
            Collab2 = input("Collaborateur 2 : ")
            print("Les collaborateurs communs de " + Collab1 + " et " + Collab2 + " sont : " + str(requettes.collaborateurs_communs(Graph,Collab1,Collab2))+"\n")
            temp = input("Voulez vous continuer ? (Y/N) : ")
            if(temp != "Y" and temp != "y"):
                run = False
            else:
                choisi=-1
                data=-1
        elif(choisi ==  2):
            print("Indiquez les collaborateurs voulus : ")
            aff = input("Souhaitez vous voir la liste des collaborateurs disponibles ? (Y/N)\n")
            if(aff == "Y" or aff == "y"):
                print(requettes.nodesInGraph(Graph))
            print("\n")
            Collab1 = input("Collaborateur 1 : ")
            Collab2 = input("Collaborateur 2 : ")
            non_valide = False
            demande = -1
            while not non_valide:
                demande = input("Souhaitez vous : \n[1] - une distance optimisée (durée d'éxécution plus longue)\n[2] - une distance non optimisée (durée d'éxécution plus rapide)\nVotre réponse : ")
                if demande.isnumeric():
                    demande = int(demande)
                    if demande == 1 or demande == 2:
                        non_valide = True
            temp = None
            if demande == 1:
                temp = requettes.collaborateurs_proches(Graph,Collab1,requettes.distance(Graph,Collab1,Collab2))
            if demande == 2:
                temp = requettes.collaborateurs_proches(Graph,Collab1,requettes.distance_naive(Graph,Collab1,Collab2))
            test = set()
            test.add(Collab1)
            if(temp == test):
                print("Ces acteurs n'ont pas de liens")
            else:
                print("Les collaborateurs proches de " + Collab1 + " et " + Collab2 + " sont : " + str(temp))
            temp = input("Voulez vous continuer ? (Y/N) : ")
            if(temp != "Y" and temp != "y"):
                run = False
            else:
                choisi=-1
                data=-1
        elif(choisi == 3):
            print("Indiquez le collaborateur souhaité : ")
            aff = input("Souhaitez vous voir la liste des collaborateurs disponibles ? (Y/N)\n")
            if(aff == "Y" or aff == "y"):
                print(requettes.nodesInGraph(Graph))
            print("\n")
            Collab1 = input("Collaborateur : ")
            print("La centralité de l'acteur + " + Collab1 + " est " + str(requettes.centralite_acteur(Graph,Collab1)))
            temp = input("Voulez vous continuer ? (Y/N) : ")
            if(temp != "Y" and temp != "y"):
                run = False
            else:
                choisi=-1
                data=-1
        elif(choisi == 4):
            print("L'éloignement le plus grand dans le graph est de " + str(requettes.eloignement_max(Graph)))
            temp = input("Voulez vous continuer ? (Y/N) : ")
            if(temp != "Y" and temp != "y"):
                run = False
            else:
                choisi=-1
                data=-1
Menu()