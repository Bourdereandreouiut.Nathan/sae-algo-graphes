import networkx as ntx
import json
import matplotlib.pyplot as plt

#Q1

def json_vers_nx(chemin):
    File1 = open(chemin, 'r')
    Data1 = json.loads(File1.read())

    G = ntx.Graph()

    for i in Data1['data_exploit']:
        if 'cast' in i.keys():
            for u in i['cast']:
                for u2 in i['cast']:
                    uClean = cleannom(u)
                    u2Clean = cleannom(u2)
                    G.add_node(uClean)
                    G.add_node(u2Clean)
                    G.add_edge(uClean,u2Clean)
    return G

#Q2
def collaborateurs_communs(G,u,v):
    resFinal = set()
    if u not in G.nodes or v not in G.nodes:
        return None
    ListAdjU = G.adj[u]
    ListAdjV = G.adj[v]
    for i in ListAdjU:
        for w in ListAdjV:
            if i == w:
                resFinal.add(i)
    if resFinal == set():
        return None
    return resFinal

#Q3
def collaborateurs_proches(G,u,k):
    """Fonction renvoyant l'ensemble des acteurs à distance au plus k de l'acteur u dans le graphe G. La fonction renvoie None si u est absent du graphe.
    
    Parametres:
        G: le graphe
        u: le sommet de départ, sera aussi appelé collaborateur initial
        k: la distance depuis u
    """
    if u not in G.nodes:
        print(u,"est un illustre inconnu") #on regarde ici le collaborateur initial, si il est dans les noeuds du graphe fourni ou non, si non on renvoie none, autrement on poursuit
        return None
    collaborateurs = set()
    collaborateurs.add(u) #on ajoute le collaborateur initial à un nouvel ensemble et on l'affiche
    for i in range(k): #on fait une boucle avec la distance depuis k
        collaborateurs_directs = set() #on crée un autre ensemble avec les collaborateurs direct à chaque distance (1,2,3,...k)
        for c in collaborateurs: #on fait une boucle unique du collaborateur initial
            for voisin in G.adj[c]: #on récupère les sommets adjacents du collaborateur initial
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin) #on ajoute le voisin si il n'est pas dans l'ensemble contenant à la base le collaborateur initial
        collaborateurs = collaborateurs.union(collaborateurs_directs) #on unit les deux ensembles par un OU logique
    return collaborateurs

def distance_naive(G,u,v):
    la_distance = 1
    fin = False
    distance_max = 6 #on considère la distance maximum du graphe à 6, car après les stars se connaissent de trop loin!s
    if u not in G.nodes or v not in G.nodes:
        return None
    deja_vu = []
    collabs = collaborateurs_proches(G,u,la_distance)
    while not fin:
        for acteur in collabs:
            if acteur not in deja_vu:
                if acteur == v:
                    fin = True
                deja_vu.append(acteur)
        la_distance += 1
        if la_distance > distance_max:
            fin = True
        collabs = collaborateurs_proches(G,u,la_distance)
    if la_distance < distance_max:
        return la_distance
    return 0
    
def distance(G,u,v): #O(|V|) présumément, à rechercher
    
    if u not in G.nodes or v not in G.nodes:
        return None
    parcouru = []
    temp = [[u]]
    while temp:
        chemin = temp.pop(0)
        arrete = chemin[-1]
        if arrete not in parcouru:
            voisins = G[arrete]
            for vwazin in voisins:
                nouveau_chemin = list(chemin)
                nouveau_chemin.append(vwazin)
                temp.append(nouveau_chemin)
                if vwazin == v:
                    return len(nouveau_chemin)
                if len(temp) > G.number_of_nodes():
                    return 0 
#Q4
def centralite_acteur(G,u):
    res = 0
    multiplicateur = 0
    if u not in G.nodes:
        return None
    collaborateurs = set()
    collaborateurs.add(u)
    for i in range(6):
        multiplicateur = i+1 
        collaborateurs_directs = set() 
        for c in collaborateurs: 
            for voisin in G.adj[c]: 
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin)
                res += multiplicateur
        collaborateurs = collaborateurs.union(collaborateurs_directs) 
    diviseur = len(G.nodes)-1
    return (u,res/diviseur)

def centre_hollywood(G):
    dicoCentralitee = dict()
    plus_central = None
    acteur_centre = None
    dicoCentreHollywood = dict()
    for source in G.nodes:
        if source not in dicoCentralitee:
            dicoCentralitee[source] = centralite_acteur(G,source)[1]
    for acteur,nombre in dicoCentralitee.items():
        if plus_central == None or plus_central > nombre:
            plus_central = nombre
            acteur_centre = acteur
    dicoCentreHollywood[acteur_centre] = plus_central
    for acteur,nombre in dicoCentralitee.items():
        if plus_central == nombre:
            dicoCentreHollywood[acteur] = nombre
    return dicoCentreHollywood

#Q5
def eloignement_max(G):
    ens = set()
    for actsource in G.nodes:
        for actrecherche in G.nodes:
                if actrecherche != actsource:
                    ens.add(distance(G,actsource,actrecherche))
    max_distance = max(ens)
    return max_distance

#Fonctions utiles
def cleannom(nom):
    if nom != None or nom != "":
        if nom[0] == '[':
            nom = nom.replace('[', '')
            nom = nom.replace(']', '')
        if nom.find('(actor)') != -1:
            nom = noDoublons(nom.replace('(actor)', ''))
        if nom.find('(actress)') != -1:
            nom = nom.replace('(actress)', '')
            nom = noDoublons(nom.replace('(actor)', ''))
        if nom.find('|') != -1:
            nom = nom.replace('|', '')
            nom = noDoublons(nom)
    return nom

def noDoublons(nom):
        newNom = ""
        for x in range(len(nom)//2):
            newNom += nom[x]
            return newNom

def nodesInGraph(G):
    listeActeurs = []
    for x in G.nodes():
        listeActeurs.append(x)
    return listeActeurs